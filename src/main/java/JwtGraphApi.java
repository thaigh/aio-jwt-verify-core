import com.asciano.azure.graphapi.GraphLookup;
import com.pacificnational.azure.federation.FederationCertificate;
import com.pacificnational.azure.federation.FederationMetadataDocument;
import com.pacificnational.azure.federation.InvalidMetadataException;
import com.pacificnational.azure.federation.MetadataReader;
import com.pacificnational.jwt.user.PacificNationalUser;
import com.pacificnational.jwt.validator.JwtValidator;
import org.json.JSONObject;

import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by tyler on 18/7/17.
 */
public class JwtGraphApi {


    public static void main(String[] args) {
        final String jwtToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IlZXVkljMVdEMVRrc2JiMzAxc2FzTTVrT3E1USIsImtpZCI6IlZXVkljMVdEMVRrc2JiMzAxc2FzTTVrT3E1USJ9.eyJhdWQiOiJodHRwczovL2dyYXBoLndpbmRvd3MubmV0IiwiaXNzIjoiaHR0cHM6Ly9zdHMud2luZG93cy5uZXQvZDkwZTg5YzUtOWFhOC00ODQyLTk3OGQtNjhhMWY5Zjc5OGVjLyIsImlhdCI6MTUwMDUzMDQxOSwibmJmIjoxNTAwNTMwNDE5LCJleHAiOjE1MDA1MzQzMTksImFjciI6IjEiLCJhaW8iOiJBU1FBMi84REFBQUF0cjBWWWZkOE9lVDdhbXdpb1RXV2g1bk1zZE9NTnhuUEdoYStCYm5yaWQ0PSIsImFtciI6WyJwd2QiXSwiYXBwaWQiOiI5YmExYTVjNy1mMTdhLTRkZTktYTFmMS02MTc4YzhkNTEyMjMiLCJhcHBpZGFjciI6IjAiLCJlX2V4cCI6MjYyODAwLCJmYW1pbHlfbmFtZSI6IkhhaWdoIiwiZ2l2ZW5fbmFtZSI6IlR5bGVyIiwiaXBhZGRyIjoiMTMuNTQuMTIzLjEwNiIsIm5hbWUiOiJIYWlnaCwgVHlsZXIiLCJvaWQiOiI0NzE1ODRkOC0xYTIxLTQ3MGQtYWExOS02NGI5NzViZjhkNzYiLCJvbnByZW1fc2lkIjoiUy0xLTUtMjEtMzcyMDI0MjgtMjc3MDA1MDYwLTE3MTAwMzc3NTMtMTg1MDkiLCJwbGF0ZiI6IjE0IiwicHVpZCI6IjEwMDNCRkZEOEJCMzVDNUQiLCJzY3AiOiJ1c2VyX2ltcGVyc29uYXRpb24iLCJzdWIiOiJSLW9pMm5sZ3ZoNlF0NGJ1NzZCcHdVeGkxZ0Y5bVdSY2M4N0duRlJ1bHBRIiwidGlkIjoiZDkwZTg5YzUtOWFhOC00ODQyLTk3OGQtNjhhMWY5Zjc5OGVjIiwidW5pcXVlX25hbWUiOiJUeWxlcl9IYWlnaEBwYWNpZmljbmF0aW9uYWwuY29tLmF1IiwidXBuIjoiVHlsZXJfSGFpZ2hAcGFjaWZpY25hdGlvbmFsLmNvbS5hdSIsInV0aSI6InpvVFhXRUNYdGtHTjVHSW5LTDBIQUEiLCJ2ZXIiOiIxLjAifQ.G5uKXAe4D2CMGEOt_UoamXBFuQW41jiI01JUe2mbk-McyeioX8d1RTNcCGm3flZ9qxDQBs_sObckQnTW3qc-zIy5ayoq4mdknu3VIAgmZg6ARArGIsOST2VbeLhi52gruArOR7U_qalc7KNoYWlQea-ykIXDagcSLrLag_pxMgbh9de2hAewtaePAWAbAcVCoR3pvizSpRdnZrqqOQnt2yMyKEtCphp-NtvPTyQ_t3x6kaP3WpqKLILP73WnFYRsxvL4XNsR2GKIixsMXWm6xZwm_6L4N4XR3TGa8xX9kQOQXwenCY8VtfLTAfQydzbhQ6dY7WwXMgEW-yzkeNG53w";
        final String azureTenantId = "d90e89c5-9aa8-4842-978d-68a1f9f798ec";
        final String apiVersion = "1.5";
        final String appPrincipalId = "8200e42a-f128-444c-bc40-87459a6aa775";
        final String symmetricKey = "0+PZ32qEqxnvddSW5tryX8NAPlQKZ33SwCFC2DVfA+I=";
        final String authUrl = "https://login.windows.net/%s/oauth2/token?api-version=1.0";

        JwtGraphApi api = new JwtGraphApi();
        JSONObject user = api.getUserObject(jwtToken, azureTenantId, apiVersion, appPrincipalId, symmetricKey, authUrl);
        System.out.println(user);

        if (user != null) {
            try {
                PacificNationalUser pnUser = PacificNationalUser.fromJSONObject(user);
                System.out.println(pnUser);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

    }

    public JSONObject getUserObject(String jwtToken, String azureTenantId, String apiVersion, String appPrincipalId, String symmetricKey, String authUrl)

    {

        JSONObject user = null;
        try {

            Map<String, Object> mp = getJwt(azureTenantId, jwtToken);
            if (mp == null) return null;

            String upn = ((String) mp.get("unique_name")).replace("\"", "");

            GraphLookup lookup = new GraphLookup();
            user = lookup.getUserAttributes(apiVersion, appPrincipalId, symmetricKey, azureTenantId, upn, authUrl);
            if (user != null)
                return user;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return user;

    }

    private Map<String, Object> getJwt(String azureTenantId, String jwtToken)
            throws IOException, InvalidMetadataException, XPathExpressionException
    {
        MetadataReader reader = new MetadataReader();
        FederationMetadataDocument doc = reader.downloadFederationMetadataFromTenantId(azureTenantId);

        JwtValidator validator = new JwtValidator();
        List<FederationCertificate> certs = doc.getAllCertificates();

        for (FederationCertificate cert : certs) {

            try {
                Map<String, Object> mp = validator.validateJwt(cert, jwtToken);
                return mp;
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        return null;
    }

}
