package com.pacificnational.jwt.user;

import org.json.*;

/**
 * Created by tyler on 19/7/17.
 */
public class PacificNationalUser {

    // User Name details

    private String userPrincipalName;
    private String givenName;
    private String surname;
    private String displayName;

    // Position Description

    private String jobTitle;
    private String department;
    private String companyName;

    // Location Description

    private String physicalDeliveryOfficeName;
    private String streetAddress;
    private String postalCode;
    private String city;
    private String state;
    private String country;

    // Contact Details

    private String mail;
    private String mailNickname;
    private String faxNumber; // Most likely a string representation
    private String mobileNumber;
    private String telephoneNumber;

    // Extension properties in Azure Graph from PeopleSoft

    private String employeeId;
    private String employeeNumber; // String represented number

    // Other Properties
    private Boolean accountEnabled;
    private String preferredLanguage;


    public PacificNationalUser() { }

    public String getUserPrincipalName() { return userPrincipalName; }
    public void setUserPrincipalName(String userPrincipalName) { this.userPrincipalName = userPrincipalName; }

    public String getGivenName() { return givenName; }
    public void setGivenName(String givenName) { this.givenName = givenName; }

    public String getSurname() { return surname; }
    public void setSurname(String surname) { this.surname = surname; }

    public String getDisplayName() { return displayName; }
    public void setDisplayName(String displayName) { this.displayName = displayName; }

    public String getJobTitle() { return jobTitle; }
    public void setJobTitle(String jobTitle) { this.jobTitle = jobTitle; }

    public String getDepartment() { return department; }
    public void setDepartment(String department) { this.department = department; }

    public String getCompanyName() { return companyName; }
    public void setCompanyName(String companyName) { this.companyName = companyName; }

    public String getPhysicalDeliveryOfficeName() { return physicalDeliveryOfficeName; }
    public void setPhysicalDeliveryOfficeName(String physicalDeliveryOfficeName) { this.physicalDeliveryOfficeName = physicalDeliveryOfficeName; }

    public String getStreetAddress() { return streetAddress; }
    public void setStreetAddress(String streetAddress) { this.streetAddress = streetAddress; }

    public String getPostalCode() { return postalCode; }
    public void setPostalCode(String postalCode) { this.postalCode = postalCode; }

    public String getCity() { return city; }
    public void setCity(String city) { this.city = city; }

    public String getState() { return state; }
    public void setState(String state) { this.state = state; }

    public String getCountry() { return country; }
    public void setCountry(String country) { this.country = country; }

    public String getMail() { return mail; }
    public void setMail(String mail) { this.mail = mail; }

    public String getMailNickname() { return mailNickname; }
    public void setMailNickname(String mailNickname) { this.mailNickname = mailNickname; }

    public String getFaxNumber() { return faxNumber; }
    public void setFaxNumber(String faxNumber) { this.faxNumber = faxNumber; }

    public String getMobileNumber() { return mobileNumber; }
    public void setMobileNumber(String mobileNumber) { this.mobileNumber = mobileNumber; }

    public String getTelephoneNumber() { return telephoneNumber; }
    public void setTelephoneNumber(String telephoneNumber) { this.telephoneNumber = telephoneNumber; }

    public String getEmployeeId() { return employeeId; }
    public void setEmployeeId(String employeeId) { this.employeeId = employeeId; }

    public String getEmployeeNumber() { return employeeNumber; }
    public void setEmployeeNumber(String employeeNumber) { this.employeeNumber = employeeNumber; }

    public Boolean getAccountEnabled() { return accountEnabled; }
    public void setAccountEnabled(Boolean accountEnabled) { this.accountEnabled = accountEnabled; }

    public String getPreferredLanguage() { return preferredLanguage; }
    public void setPreferredLanguage(String preferredLanguage) { this.preferredLanguage = preferredLanguage; }

    public static PacificNationalUser fromJSONObject(JSONObject object) throws JSONException {

        PacificNationalUser user = new PacificNationalUser();

        // User name Details

        user.userPrincipalName = getProp(String.class, object, "userPrincipalName");
        user.givenName = getProp(String.class, object, "givenName");
        user.surname = getProp(String.class, object, "surname");
        user.displayName = getProp(String.class, object, "displayName");

        // Position Description

        user.jobTitle = getProp(String.class, object, "jobTitle");
        user.department = getProp(String.class, object, "department");
        user.companyName = getProp(String.class, object, "companyName");

        // Location Description

        user.physicalDeliveryOfficeName = getProp(String.class, object, "physicalDeliveryOfficeName");
        user.streetAddress = getProp(String.class, object, "streetAddress");
        user.postalCode = getProp(String.class, object, "postalCode");
        user.city = getProp(String.class, object, "city");
        user.state = getProp(String.class, object, "state");
        user.country = getProp(String.class, object, "country");

        // Contact Details

        user.mail = getProp(String.class, object, "mail");
        user.mailNickname = getProp(String.class, object, "mailNickname");
        user.faxNumber = getProp(String.class, object, "facsimileTelephoneNumber");
        user.mobileNumber = getProp(String.class, object, "mobile");
        user.telephoneNumber = getProp(String.class, object, "telephoneNumber");

        user.employeeId = getProp(String.class, object, "extension_employeeId");
        user.employeeNumber = getProp(String.class, object, "extension_employeeNumber");

        // Other Properties
        user.accountEnabled = getProp(Boolean.class, object, "accountEnabled");
        user.preferredLanguage = getProp(String.class, object, "preferredLanguage");

        return user;
    }

    private static <T> T getProp(Class<T> cls, JSONObject obj, String key) {
        if (!obj.has(key)) return null;

        try {
            if (obj.isNull(key)) return null;

            Object val = obj.get(key);
            return (T)val;
        } catch (JSONException e) {
            return null;
        }

    }
}
